﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalScore : MonoBehaviour {

	[SerializeField] private Text finalScoreText;

	void OnEnable()
	{
		Messenger.AddListener<int> ("FinalScore", UpdateScore);
	}

	void OnDisable()
	{
		Messenger.RemoveListener<int> ("FinalScore", UpdateScore);
	}

	// Use this for initialization
	void Start ()
	{
	}

	void UpdateScore(int score)
	{
		finalScoreText.text = "Final Score :" + score.ToString ();
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
