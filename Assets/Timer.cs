﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	private const float orderTimeLimit = 8;				
	private const float lineTimeLimit = 5;
	public float lineTimeRemaining;
	private Text timerText;

	private Image circleImage;

	private bool isThereACustomer = false;
	private bool gameStarted = false;	

	void OnEnable()
	{
		Messenger.AddListener ("ResetTimer", ResetOrder);
		Messenger.AddListener ("StopTimer", StopOrderTimer);
		Messenger.AddListener ("StartTimer", StartOrderTimer);
		Messenger.AddListener ("StartGame", GameStarted);
		Messenger.AddListener ("GameOver", GameFinished);
	}

	void OnDisable()
	{
		Messenger.RemoveListener ("ResetTimer", ResetOrder);
		Messenger.RemoveListener ("StopTimer", StopOrderTimer);
		Messenger.RemoveListener ("StartGame", StartOrderTimer);
		Messenger.RemoveListener ("StartGame", GameStarted);
		Messenger.RemoveListener ("GameOver", GameFinished);
	}

	// Use this for initialization
	void Start ()
	{

		//timerText = GetComponent<Text> ();
		circleImage = GetComponent<Image> ();
		circleImage.fillAmount = 1f;

		isThereACustomer = false;
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (gameStarted) {

			lineTimeRemaining -= Time.deltaTime;
		}
		if (isThereACustomer)
		{

			circleImage.fillAmount -= Time.deltaTime / orderTimeLimit;

		}

		//timerText.text = "Time Remaining :" + orderTimeRemaining.ToString ();

		if (circleImage.fillAmount == 0f) 
		{
			Messenger.Broadcast("SpawnNewCup");
            Messenger.Broadcast("RemoveOldCup");
			Messenger.Broadcast("RemoveFromQueue");
			Messenger.Broadcast ("IncreaseStrikes");
		}

		if (lineTimeRemaining < 0)
		{
			Messenger.Broadcast("SpawnCharacter");
			ResetLine();
		}
	}

	void ResetOrder()
	{
		circleImage.fillAmount = 1f;

	}

	void ResetLine()
	{
		lineTimeRemaining = lineTimeLimit;
	}

	void StopOrderTimer()
	{
		isThereACustomer = false;
		circleImage.fillAmount = 1f;
	}

	void StartOrderTimer()
	{

		isThereACustomer = true;
	}

	void GameStarted()
	{
		gameStarted = true;
		ResetLine ();
		StartOrderTimer ();
	}

	void GameFinished()
	{
		gameStarted = false;
		StopOrderTimer ();
	}
}
