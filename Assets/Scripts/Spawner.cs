﻿using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject sugarCube;

    private Color coffeeColor;

    private ParticleSystem coffeeParticle; 

    void Awake()
    {

    }

    void Start()
    {
        coffeeParticle = GetComponent<ParticleSystem>();
        coffeeColor = coffeeParticle.startColor;
    }

    void OnEnable()
    {
        Messenger.AddListener("AddCoffee", SpawnCoffee);
        Messenger.AddListener("AddSugar", SpawnSugar);
        Messenger.AddListener("AddMilk", SpawnMilk);
      
    }

    void OnDisable()
    {
        Messenger.RemoveListener("AddCoffee", SpawnCoffee);
        Messenger.RemoveListener("AddSugar", SpawnSugar);
        Messenger.RemoveListener("AddMilk", SpawnMilk);
        
    }

    void SpawnCoffee()
    {
        coffeeParticle.startColor = coffeeColor;
        coffeeParticle.Play();
    }

    void SpawnSugar()
    {
        Instantiate(sugarCube, this.transform.position, Quaternion.identity);
    }

    void SpawnMilk()
    {
        coffeeParticle.startColor = Color.white;
        coffeeParticle.Play();
    }

    
};