﻿using UnityEngine;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {

	private string currentState;
	void OnEnable()
	{
		Messenger.AddListener<string> ("GameStateUpdate", UpdateGameState);
	}

	void OnDisable()
	{
		Messenger.RemoveListener<string> ("GameStateUpdate", UpdateGameState);
	}
	
	// Use this for initialization
	void Start ()
    {
		currentState = "STARTSCREEN";
    }

	void UpdateGameState(string _state)
	{
		currentState = _state;
	}

	// Update is called once per frame
	void Update ()
    {
        
		if (currentState == "PLAY") {
			//Size changes
			if (Input.GetKeyDown (KeyCode.LeftArrow)) 
            {
				Messenger.Broadcast ("DecreaseSize");
			}
            else if (Input.GetKeyDown (KeyCode.RightArrow))
            { 
				Messenger.Broadcast ("IncreaseSize");
			}
            
            else if (Input.GetKeyDown (KeyCode.Space)) {
				//Check the current cup
				Messenger.Broadcast ("GiveCoffee");
				Messenger.Broadcast ("CheckCup");
				Messenger.Broadcast ("RemoveFromQueue");
                Messenger.Broadcast("RemoveOldCup");
				
			} else if (Input.GetKeyDown (KeyCode.C)) {
				
				Messenger.Broadcast ("AddCoffee");
			} else if (Input.GetKeyDown (KeyCode.M)) {
				
				Messenger.Broadcast ("AddMilk");
			} else if (Input.GetKeyDown (KeyCode.S)) {
				
				Messenger.Broadcast ("AddSugar");
			}
		}

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch(currentState)
            {
                case "STARTSCREEN":
                    Messenger.Broadcast("TutorialHUD");
                    break;
                case "TUTHUD":
                    Messenger.Broadcast("TutorialWorkStation");
                    break;
                case "TUTWORKSTATION":
                    Messenger.Broadcast("TutorialCustomer");
                    break;
                case "TUTCUSTOMER":
                case "GAMEOVER":
                    Messenger.Broadcast("StartGame");
                    break;
               
               
            }
        }
    }
}
