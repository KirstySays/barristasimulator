﻿using UnityEngine;
using System.Collections.Generic;

public class Order : MonoBehaviour
{
    public enum CoffeeSize { SMALL, MEDIUM, LARGE };
    public enum Milk { NONE, NORMAL, FROTHCAP, FROTHLATTE, };
    public enum Flavour { NONE, GINGERBREAD, HAZELNUT, CARAMEL, MINT };

    public struct OrderType
    {
        public CoffeeSize size;
        public Milk milk;
        public Flavour flavour;
        public int coffee;
        public int sugar;
    }


    private OrderType order;
    private OrderType coffeeMade;

    private int milkDifficulty;
	private int strikes;
	private const int maxStrikes = 3;

	private bool queueFilled;

    void Start()
    {
		milkDifficulty = 2;
		strikes = 0;

    }

    void OnEnable()
    {
 
        Messenger.AddListener("CheckCup", CheckOrder);
      	Messenger.AddListener("SpawnNewCup", NewCup);
        Messenger.AddListener("GenerateOrder", GenerateNewOrder);
        Messenger.AddListener("DecreaseSize", DecreaseSize);
        Messenger.AddListener("IncreaseSize", IncreaseSize);
        Messenger.AddListener("AddCoffee", AddCoffee);
        Messenger.AddListener("AddMilk", AddMilk);
        Messenger.AddListener("AddSugar", AddSugar);
        Messenger.AddListener("IncreaseDiff", IncreaseDifficulty);
		Messenger.AddListener ("StartGame", StartGame);
		Messenger.AddListener ("QueueEmpty", QueueEmpty);
		Messenger.AddListener ("QueueFilled", QueueFilled);

    }

    void OnDisable()
    {
        Messenger.RemoveListener("CheckCup", CheckOrder);
        Messenger.RemoveListener("SpawnNewCup", NewCup);
        Messenger.RemoveListener("GenerateOrder", GenerateNewOrder);
        Messenger.RemoveListener("DecreaseSize", DecreaseSize);
        Messenger.RemoveListener("IncreaseSize", IncreaseSize);
        Messenger.RemoveListener("AddCoffee", AddCoffee);
        Messenger.RemoveListener("AddMilk", AddMilk);
        Messenger.RemoveListener("AddSugar", AddSugar);
        Messenger.RemoveListener("IncreaseDiff", IncreaseDifficulty);
		Messenger.RemoveListener ("StartGame", StartGame);
		Messenger.RemoveListener ("QueueEmpty", QueueEmpty);
		Messenger.RemoveListener ("QueueFilled", QueueFilled);

    }

	void StartGame()
	{
		NewCup();
		GenerateNewOrder();
		queueFilled = true;
		strikes = 0;
	}

    void GenerateNewOrder()
    {

        order = new OrderType();

        int sizeRand = Random.Range(0, 3);
        order.size = (CoffeeSize)sizeRand;

        int milkRand = Random.Range(0, milkDifficulty);      //Need to change this dependant on difficulty
		Debug.Log ("Milk random : " + milkRand);
        order.milk = (Milk)milkRand;

        order.flavour = Flavour.NONE;

        int shotRand = Random.Range(1, 3);
        order.coffee = shotRand;

        int sugarRand = Random.Range(0, 5);
        order.sugar = sugarRand;

        Messenger.Broadcast<OrderType>("UpdateOrderGUI", order);
		Messenger.Broadcast ("ResetTimer");
        
    }

   
    void CheckOrder()
    {

       if (queueFilled) {

			if (coffeeMade.coffee == order.coffee
				&& coffeeMade.flavour == order.flavour
				&& coffeeMade.milk == order.milk
				&& coffeeMade.size == order.size
				&& coffeeMade.sugar == order.sugar) {
				Messenger.Broadcast<bool> ("OrderComplete", true);
            
			} else {
				strikes++;

				if (strikes >= maxStrikes)
					Messenger.Broadcast ("GameOver");
				else {
					Messenger.Broadcast ("IncreaseStrikes");
					Messenger.Broadcast<bool> ("OrderComplete", false);
				}
            
			}

			NewCup ();
		}
    }

    void DecreaseSize()
    {
       if (!CheckContents())
        {
            if (coffeeMade.size == CoffeeSize.SMALL || coffeeMade.size == CoffeeSize.MEDIUM)
            {
                coffeeMade.size = CoffeeSize.SMALL;

            }
            else if (coffeeMade.size == CoffeeSize.LARGE)
            {
                coffeeMade.size = CoffeeSize.MEDIUM;

            }
            Messenger.Broadcast<Order.CoffeeSize>("DecreaseVisual", coffeeMade.size);
        }

       
    }

    void IncreaseSize()
    {
        if (!CheckContents())
        {
            if (coffeeMade.size == CoffeeSize.SMALL)
            {
                coffeeMade.size = CoffeeSize.MEDIUM;

            }

            else if (coffeeMade.size == CoffeeSize.MEDIUM || coffeeMade.size == CoffeeSize.LARGE)
            {
                coffeeMade.size = CoffeeSize.LARGE;

            }

            Messenger.Broadcast<Order.CoffeeSize>("IncreaseVisual", coffeeMade.size);
        }

    }

    bool CheckContents()
    {
        if (coffeeMade.coffee > 0)
            return true;
        else if (coffeeMade.milk != (Milk)0)
            return true;
        else if (coffeeMade.sugar > 0)
            return true;
        else
            return false;
    }
    void AddCoffee()
    {
        coffeeMade.coffee++;
        
    }

    void AddMilk()
    {
        coffeeMade.milk = Milk.NORMAL;
       
    }

    void AddSugar()
    {
        coffeeMade.sugar++;
       
    }
    void NewCup()
    {
        //Get a new cup made. 
        coffeeMade = new OrderType();

        coffeeMade.size = CoffeeSize.MEDIUM;
        coffeeMade.milk = Milk.NONE;
        coffeeMade.flavour = Flavour.NONE;
        coffeeMade.coffee = 0;
        coffeeMade.sugar = 0;   
    }


    void IncreaseDifficulty()
    {
        milkDifficulty++;
    }

	void QueueFilled()
	{
		queueFilled = true;
	}

	void QueueEmpty()
	{
		queueFilled = false;
	}
}