﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterVisuals : MonoBehaviour {

	List<GameObject> currentLine;
	private Vector3 characterOffset = new Vector3(0,0, 1);
	private Vector3 backOfQueue;
	[SerializeField] private int maxQueueLength = 1;

    void OnEnable()
    {
		Messenger.AddListener ("StartGame", Reset);
		Messenger.AddListener ("RemoveFromQueue", DeleteCharacter);
		Messenger.AddListener("SpawnCharacter", SpawnCharacter);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("SpawnCharacter", SpawnCharacter);
		Messenger.RemoveListener ("RemoveFromQueue", DeleteCharacter);
		Messenger.RemoveListener ("StartGame", Reset);
    }

	void Start()
	{
		currentLine = new List<GameObject> ();
	}

	// Use this for initialization
	void Reset () 
	{
		if (currentLine.Count > 0)
		{
			DestroyCharacters();
		}
		currentLine.Clear ();
		backOfQueue = transform.position;

		SpawnCharacter ();
	}

	void DestroyCharacters()
	{
		for (int index = 0; index < currentLine.Count; index++) 
		{
			Destroy(currentLine[index].gameObject);
		}
	
	}
    void SpawnCharacter()
    {
        int randChar = Random.Range(1, 4);


		//Is this customer the first in the queue?
		if (currentLine.Count == 0) {
			
			currentLine.Add (Instantiate (Resources.Load ("Character" + randChar), transform.position, Quaternion.identity) as GameObject);
			Messenger.Broadcast ("GenerateOrder");				//Display a new order otherwise leave it. 
			Messenger.Broadcast ("StartTimer");

		} else {
			Vector3 tempStartPos = backOfQueue + characterOffset;
			currentLine.Add(Instantiate(Resources.Load("Character" + randChar), tempStartPos, Quaternion.identity) as GameObject);
			backOfQueue = tempStartPos;
		}

		CheckQueueLength ();
    }

	void CheckQueueLength()
	{
		if (currentLine.Count > maxQueueLength)
		{
			Messenger.Broadcast("GameOver");
		}
	}

	void DeleteCharacter()
	{

		if (currentLine.Count == 1)
		{
			Destroy (currentLine [0]);
			currentLine.RemoveAt (0);
			Messenger.Broadcast("StopTimer");
			Messenger.Broadcast ("ClearOrder");
			Messenger.Broadcast ("QueueEmpty");
		}

		//Is there someone still in the queue? If So, we need a new order
		else if (currentLine.Count >= 1)
		{
			Destroy (currentLine [0]);
			currentLine.RemoveAt (0);
			MoveQueue ();
			Messenger.Broadcast ("GenerateOrder");
			Messenger.Broadcast("QueueFilled");

		}
	}

	void MoveQueue()
	{
		for (int index = 0; index < currentLine.Count; index++) 
		{
			currentLine[index].transform.position -= characterOffset;
		}
		backOfQueue -= characterOffset;
	}
}
