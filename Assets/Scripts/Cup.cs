﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;


public class Cup : MonoBehaviour {

    private GameObject currentModel;
    private GameObject smallCup;
    private GameObject medCup;
    private GameObject largeCup;

	private Vector3 startingPosition;

    void OnEnable()
    {
       
        //Messenger.AddListener<Order.CoffeeSize>("DecreaseVisual", DecreaseSize);
        //Messenger.AddListener<Order.CoffeeSize>("IncreaseVisual", IncreaseSize);
        Messenger.AddListener<string>("PlaceCup", PlaceCup);
        Messenger.AddListener("RemoveOldCup", RemoveCup);
    }

    void OnDisable()
    {
        
       // Messenger.RemoveListener<Order.CoffeeSize>("DecreaseVisual", DecreaseSize);
       // Messenger.RemoveListener<Order.CoffeeSize>("IncreaseVisual", IncreaseSize);
        Messenger.RemoveListener<string>("PlaceCup", PlaceCup);
        Messenger.RemoveListener("RemoveOldCup", RemoveCup);
       
    }

    void Start()
    {
        smallCup = Resources.Load("CoffeeCupSmall") as GameObject;
        medCup = Resources.Load("CoffeeCupMed") as GameObject;
        largeCup = Resources.Load("CoffeeCupLarge") as GameObject;

		startingPosition = transform.position;
		startingPosition.y -= 15;
		//currentModel = Instantiate(medCup, startingPosition, Quaternion.identity) as GameObject ;
    }

    void RemoveCup()
    {
        Destroy(currentModel);
    }

    void PlaceCup(string cupSize)
    {
        //Clean up anything from the previous order
        Destroy(currentModel);

        //Select the current size needed.
        switch (cupSize)
        {
            case "CoffeeCupSmall":
                currentModel = Instantiate(smallCup, startingPosition, Quaternion.identity) as GameObject;
                break;
            case "CoffeeCupMed":
                currentModel = Instantiate(medCup, startingPosition, Quaternion.identity) as GameObject;
                break;
            case "CoffeeCupLarge":
                currentModel = Instantiate(largeCup, startingPosition, Quaternion.identity) as GameObject;
                break;
            default:
                Debug.LogError("Place Cup received an incorrect size from the Broadcast : " + cupSize);
                break;
        }

		
    }


}
