﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour {


    private Text UIText;
	private int score;
	[SerializeField] private Text strikeText;
	[SerializeField] private Transform gameOverPosition;

	private const int scoreIncrement = 10;
    void Start()
    {

        //Get the UI components
        UIText = GetComponent<Text>();

		score = 0;
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnEnable()
    {
        Messenger.AddListener<bool> ("OrderComplete", UpdateUI);
		Messenger.AddListener ("SendFinalScore", FinalScore);
		Messenger.AddListener ("IncreaseStrikes", UpdateStrikes);
		Messenger.AddListener ("StartGame", Reset);
    }

    void OnDisable()
    {
        Messenger.RemoveListener<bool>("OrderComplete", UpdateUI);
		Messenger.RemoveListener ("IncreaseStrikes", UpdateStrikes);
		Messenger.RemoveListener ("SendFinalScore", FinalScore);
		Messenger.RemoveListener ("StartGame", Reset);
    }

	void UpdateStrikes()
	{
		strikeText.text += " X ";
	}


    void UpdateUI(bool success)	
    {
        if (success)
			score += scoreIncrement;
        
		UIText.text = "Score : " + score.ToString ();
    } 

	void FinalScore()
	{
		Messenger.Broadcast<int> ("FinalScore", score);
	}

	void Reset()
	{
		score = 0;
		UIText.text = "Score : " + score.ToString ();
		strikeText.text= "Incorrect Orders ";
	}
}
