﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class OrderGUI: MonoBehaviour
{

    private Text UIText;

    void Start()
    {
        //Get the UI components
        UIText = GetComponent<Text>();
    }
    void OnEnable()
    {
        Messenger.AddListener<Order.OrderType>("UpdateOrderGUI", UpdateUI);
		Messenger.AddListener ("ClearOrder", ClearUI);
    }

    void OnDisable()
    {
        Messenger.RemoveListener<Order.OrderType>("UpdateOrderGUI", UpdateUI);
		Messenger.RemoveListener ("ClearOrder", ClearUI);
    }


    void UpdateUI(Order.OrderType order)
    {
        string size = "";
        if (order.size == Order.CoffeeSize.SMALL)
            size = "small";
        else if (order.size == Order.CoffeeSize.MEDIUM)
            size = "medium";
        else if (order.size == Order.CoffeeSize.LARGE)
            size = "large";

        UIText.text = "Size " + order.size.ToString() + "  / Coffee " + order.coffee +
           " / Sugar " + order.sugar +  " / Milk " + order.milk;
    }

	void ClearUI()
	{
		UIText.text = "";
	}
}