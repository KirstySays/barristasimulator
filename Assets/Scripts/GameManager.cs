﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public enum gameState {DEFAULT, PLAY, TUTHUD, TUTWORKSTATION, TUTCUSTOMER, STARTSCREEN, GAMEOVER};
	private gameState currentState;
	[SerializeField] private GameObject introPanel;
	[SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject tutorialHUDPanel;
    [SerializeField] private GameObject tutorialWorkStationPanel;
    [SerializeField] private GameObject tutorialCustomerPanel;

    void OnEnable()
	{
		Messenger.AddListener ("GameOver", FinishGame);
		Messenger.AddListener ("StartGame", StartGame);
        Messenger.AddListener("TutorialHUD", TutorialHUD);
        Messenger.AddListener("TutorialWorkStation", TutorialWorkStation);
        Messenger.AddListener("TutorialCustomer", TutorialCustomer);
    }

	void OnDisable()
	{
		Messenger.RemoveListener ("GameOver", FinishGame);
		Messenger.RemoveListener ("StartGame", StartGame);
        Messenger.RemoveListener("TutorialHUD", TutorialHUD);
        Messenger.RemoveListener("TutorialWorkStation", TutorialWorkStation);
        Messenger.RemoveListener("TutorialCustomer", TutorialCustomer);
    }

	// Use this for initialization
	void Start () 
	{
		currentState = gameState.STARTSCREEN;
        tutorialCustomerPanel.SetActive(false);
        tutorialHUDPanel.SetActive(false);
        tutorialWorkStationPanel.SetActive(false);		
		Messenger.Broadcast<string> ("GameStateUpdate", currentState.ToString ());
	}


	void FinishGame()
	{
		currentState = gameState.GAMEOVER;
		gameOverPanel.SetActive (true);
		Messenger.Broadcast ("SendFinalScore");
		Messenger.Broadcast<string> ("GameStateUpdate", currentState.ToString ());
	}

	void StartGame()
	{
		currentState = gameState.PLAY;
        tutorialCustomerPanel.SetActive(false);
		gameOverPanel.SetActive (false);
		Messenger.Broadcast<string> ("GameStateUpdate", currentState.ToString ());

	}

    void TutorialHUD()
    {
        currentState = gameState.TUTHUD;
        introPanel.SetActive(false);
        tutorialHUDPanel.SetActive(true);
        Messenger.Broadcast<string>("GameStateUpdate", currentState.ToString());
    }

    void TutorialWorkStation()
    {
        currentState = gameState.TUTWORKSTATION;
        tutorialHUDPanel.SetActive(false);
        tutorialWorkStationPanel.SetActive(true);

        Messenger.Broadcast<string>("GameStateUpdate", currentState.ToString());
    }

    void TutorialCustomer()
    {
        currentState = gameState.TUTCUSTOMER;
        tutorialWorkStationPanel.SetActive(false);
        tutorialCustomerPanel.SetActive(true);
        Messenger.Broadcast<string>("GameStateUpdate", currentState.ToString());
    }
}
