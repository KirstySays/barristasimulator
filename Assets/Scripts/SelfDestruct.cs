﻿/*
 * To be used as a timer for when the object should be destroyed. 
 */


using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {

	[SerializeField] private float timeAwake;				//Time that the object will have to be alive.
	private float currentTime;

	
	// Update is called once per frame
	void Update () 
	{
		currentTime += Time.deltaTime;

		if (currentTime > timeAwake) {
			Destroy(gameObject);
		}
	}
}
